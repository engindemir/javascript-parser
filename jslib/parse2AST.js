function parse(input, rule, args) {
  args = args || [];
  return ES5Parser.matchAll(input, rule, args, function(parser, idx) {
	if (!idx || idx < 0) idx = input.length - 1;
	var spaces = new Array(input.length);
	spaces[idx] = "^";
	return "failed to match: '" +input[idx] + "' in " +
	  input.slice(0,idx) + "_" + input[idx] + "_" + input.slice(idx+1);
  });
}

function parse2AST(input,position) {
  ES5Parser.generatePositionInfo = position;
  return JSON.stringify(parse(input, 'Program', []), undefined, 2);
}

function parse2JSON(input,position) {
	  ES5Parser.generatePositionInfo = position;
	  return parse(input, 'Program', []);
	}
