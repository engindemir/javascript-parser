package en;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import en.ast.*;


public class ASTBuilder {
	
	
	private static Expr buildExprAST(Node n, HashMap<String, ArrayList<ASTNode>> idxAST) throws JSParsingException {
		return (Expr) buildFromXMLRec((Element) n, idxAST);
	}

	private static Stmt buildStmtAST(Node n, HashMap<String, ArrayList<ASTNode>> idxAST) throws JSParsingException {
		ASTNode ast = buildFromXMLRec((Element) n,  idxAST);
		if (ast instanceof Expr) {
			if(ast instanceof Literal)
				return new EmptyStmt();
			else
				return new ExprStmt((Expr) ast);
		} else
			return (Stmt) ast;
	}

	private static ASTNode buildFromXMLRec(Element n, HashMap<String, ArrayList<ASTNode>> idxAST) throws JSParsingException {
		String name = n.getNodeName();
		NodeList children = n.getChildNodes();
		int nbChildren = children == null ? 0 : children.getLength();
		
		ASTNode ast;

		if (name.equals("Program")) {
			List<Stmt> l = new ArrayList<Stmt>();

			for (int i = 0; i < nbChildren; ++i)
				l.add(buildStmtAST(children.item(i),idxAST));

			ast = new Program(l);
		} else if (name.equals("IdExpr")) {
			ast = new IdExpr(n.getAttribute("name"));
		} else if (name.equals("LiteralExpr")) {
			String value=n.getAttribute("value");
			String type=n.getAttribute("type");
			
			if(!type.equals("string") && (value.equals("true") || value.equals("false"))) {
				ast = new BoolLiteral(Boolean.parseBoolean(value));
			} else if(!type.equals("string") && value.matches("-?[0-9]+")) {
				ast = new NumericLiteral(Integer.parseInt(value));
			} else {
				ast = new StringLiteral(value);
			}
		} else if (name.equals("ConditionalExpr")) {
			ast = new CondExpr(buildExprAST(children.item(0),idxAST),
					buildExprAST(children.item(1),idxAST),
					buildExprAST(children.item(2),idxAST));
			idxAST.get("CondExpr").add(ast);
		} else if (name.equals("UnaryExpr") || name.equals("CountExpr")) {
			// We ignore the prefix/postfix character of the count expression
			ast = new UnaryExpr(new Op(n.getAttribute("op")),
					buildExprAST(children.item(0),idxAST));
		} else if (name.equals("BinaryExpr")) {
			ast = new BinaryExpr(new Op(n.getAttribute("op")),
					buildExprAST(children.item(0),idxAST),
					buildExprAST(children.item(1),idxAST));
		} else if (name.equals("FunctionExpr") || name.equals("FunctionDecl")) {
			ast = new FunctionDecl((IdExpr) buildExprAST(children.item(0),idxAST),
					(ParamDecl) buildFromXMLRec((Element) children.item(1),idxAST),
					buildStmtAST(children.item(2),idxAST));
			idxAST.get("FunctionDecl").add(ast);
		} else if (name.equals("ParamDecl")) {
			List<String> l = new ArrayList<String>();

			for (int i = 0; i < nbChildren; ++i) {
				l.add(((Element) children.item(i)).getAttribute("name"));
			}

			ast = new ParamDecl(l);
		} else if (name.equals("VarDecl")) {
			// Variable declarations are replaced with variable initializations
			List<AssignExpr> l = new ArrayList<AssignExpr>();
			for (int i = 0; i < nbChildren; ++i) {
				// Declarations without initializations are ignored
				if (children.item(i).getNodeName().equals("IdPatt"))
					continue;

				NodeList parameters = children.item(i).getChildNodes();
				l.add(new AssignExpr(new Op("="), new IdExpr(
						((Element) parameters.item(0)).getAttribute("name")),
						buildExprAST(parameters.item(1),idxAST)));
			}
			if(l.isEmpty())
				ast = new StringLiteral("");
			else
				ast = new VarDecl(l);
		} else if (name.equals("AssignExpr")) {
			ast = new AssignExpr(new Op(n.getAttribute("op")),
					(LValue) buildExprAST(children.item(0),idxAST),
					buildExprAST(children.item(1),idxAST));
		} else if (name.equals("LogicalAndExpr")) {
			ast = new LogicalAndExpr(buildExprAST(children.item(0),idxAST),
					buildExprAST(children.item(1),idxAST));
		} else if (name.equals("LogicalOrExpr")) {
			ast = new LogicalOrExpr(buildExprAST(children.item(0),idxAST),
					buildExprAST(children.item(1),idxAST));
		} else if (name.equals("NewExpr")) {
			List<Expr> l = new ArrayList<Expr>();
			for (int i = 1; i < nbChildren; ++i)
				l.add(buildExprAST(children.item(i),idxAST));
			ast = new NewExpr(buildExprAST(children.item(0),idxAST), l);
		} else if (name.equals("CallExpr")) {
			List<Expr> l = new ArrayList<Expr>();

			for (int i = 1; i < nbChildren; ++i)
				l.add(buildExprAST(children.item(i),idxAST));

			ast = new FCallExpr(buildExprAST(children.item(0),idxAST), l);
			idxAST.get("FCallExpr").add(ast);
		} else if (name.equals("MemberExpr")) {
			ast = new MemberExpr(buildExprAST(children.item(0),idxAST),
					buildExprAST(children.item(1),idxAST));
		} else if (name.equals("ArrayExpr") || name.equals("ObjectExpr")
				|| name.equals("RegExpExpr") || name.equals("EvalExpr")) {
			// Array, object expressions, regular expressions, evals are ignored
			ast = new StringLiteral("");
		} else if (name.equals("Empty")) {
			ast = null;
		} else if (name.equals("BlockStmt")) {
			List<Stmt> l = new ArrayList<Stmt>();

			for (int i = 0; i < nbChildren; ++i) {
				l.add(buildStmtAST(children.item(i),idxAST));
			}

			ast = new BlockStmt(l);
		} else if (name.equals("LabelledStmt")) {
			// We forget about the label
			ast = buildStmtAST(children.item(0),idxAST);
		} else if (name.equals("IfStmt")) {
			ast = new IfStmt(buildExprAST(children.item(0),idxAST),
					buildStmtAST(children.item(1),idxAST),
					buildStmtAST(children.item(2),idxAST));
			idxAST.get("IfStmt").add(ast);
		} else if (name.equals("SwitchStmt")) {
			// Transform it into an if stmt, assuming that no case if visited
			// twice
			// (i.e., there is an explicit break after each case)

			// We transform the instructions of the first case into a block

			Element e = (Element) (children.item(1)).cloneNode(true);
			Element block = n.getOwnerDocument().createElement("BlockStmt");
			boolean defaultCase = children.item(1).getNodeName()
					.equals("DefaultCase");
			int indexChild = defaultCase ? 0 : 1;
			while (e.hasChildNodes()
					&& e.getChildNodes().getLength() > indexChild)
				block.appendChild(e.getChildNodes().item(indexChild));

			if (defaultCase)
				ast = buildStmtAST(block,idxAST);

			Expr headExpr = buildExprAST(children.item(0),idxAST);

			Stmt elseStmt;

			if (nbChildren > 2) {
				Element elseEl = (Element) n.cloneNode(true);
				elseEl.removeChild(elseEl.getChildNodes().item(1));
				elseStmt = buildStmtAST(elseEl,idxAST);
			} else
				elseStmt = new EmptyStmt();

			ast = new IfStmt(new BinaryExpr(new Op("=="), headExpr,
					buildExprAST(children.item(1).getChildNodes().item(0),idxAST)),
					buildStmtAST(block,idxAST), elseStmt);
			idxAST.get("IfStmt").add(ast);
		} else if (name.equals("WhileStmt")) {
			ast = new WhileStmt(buildExprAST(children.item(0),idxAST),
					buildStmtAST(children.item(1),idxAST));
		} else if (name.equals("DoWhileStmt")) {
			List<Stmt> l = new ArrayList<Stmt>();
			Stmt content = buildStmtAST(children.item(0),idxAST);
			l.add(content);
			l.add(new WhileStmt(buildExprAST(children.item(1),idxAST), content));
			ast = new BlockStmt(l);
		} else if (name.equals("TryStmt")) {
			// We ignore the catch block, and just put in the main block and the
			// finally clause
			List<Stmt> l = new ArrayList<Stmt>();
			l.add(buildStmtAST(children.item(0),idxAST));
			if (nbChildren >= 3)
				l.add(buildStmtAST(children.item(2),idxAST));
			ast = new BlockStmt(l);
		} else if (name.equals("EmptyStmt")) {
			ast = new EmptyStmt();
		} else if (name.equals("ThisExpr")) {
			ast = new ThisExpr();
		} else if (name.equals("ForStmt")) {
			ast = new ForStmt(buildExprAST(children.item(0),idxAST),
					buildExprAST(children.item(1),idxAST),
					buildExprAST(children.item(2),idxAST),
					buildStmtAST(children.item(3),idxAST));
		} else if (name.equals("ReturnStmt")) {
			ast = new ReturnStmt(buildExprAST(children.item(0),idxAST));
		} else if (name.equals("BreakStmt") || name.equals("ContinueStmt")
				|| name.equals("ForInStmt") || name.equals("WithStmt")
				|| name.equals("ThrowStmt")) {
			// These statements are not supported
			ast = new EmptyStmt();
		} else {
			throw new JSParsingException("Unsupported node type: " + name);
		}

		if (n.hasAttribute("source")) {
			ast.setLocation(n.getAttribute("source"),
					Integer.parseInt(n.getAttribute("startLine")),
					Integer.parseInt(n.getAttribute("startColumn")),
					Integer.parseInt(n.getAttribute("endLine")),
					Integer.parseInt(n.getAttribute("endColumn")));
		}
		
		return ast;
	}
	
	public static Program buildFromXML(Document d) throws JSParsingException {
		HashMap<String, ArrayList<ASTNode>> idxAST = new HashMap<String, ArrayList<ASTNode>>();		
		// keys are the name of classes that we are interested in
		idxAST.put("FunctionDecl", new ArrayList<ASTNode>());
		idxAST.put("CondExpr", new ArrayList<ASTNode>());
		idxAST.put("IfStmt", new ArrayList<ASTNode>());
		idxAST.put("FCallExpr", new ArrayList<ASTNode>());
				
		Program p =	(Program) buildFromXMLRec(d.getDocumentElement(),idxAST);
			
		p.simplify();
		p.setASTObjects(idxAST);
		
		
		return p;
	}
}
