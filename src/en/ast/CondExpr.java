package en.ast;

public class CondExpr extends Expr {

	private Expr condition;
	private Expr ifexpr;
	private Expr elseexpr;

	public CondExpr(Expr c, Expr i, Expr e) {
		condition = c;
		ifexpr = i;
		elseexpr = e;
	}

	public String toString() {
		return condition.toString() + "?" + ifexpr.toString() + ","
				+ elseexpr.toString();
	}
	
	@Override
	protected void setDepth(int d) {
		depth=d;
		ifexpr.setDepth(d);
		elseexpr.setDepth(d);
	}
	
	@Override
	public boolean contains(String str) {
		if ((condition != null) && condition.contains(str))
			return true;
		if ((ifexpr != null) && ifexpr.contains(str))
				return true;
		if ((elseexpr != null) && elseexpr.contains(str))
			return true;
		return false;	
		
	}		
}
