package en.ast;

public class IfStmt extends Stmt {

	private Expr cond;
	private Stmt ifs;
	private Stmt elses;

	public IfStmt(Expr c, Stmt i, Stmt e) {
		cond = c;
		ifs = i;
		elses = e instanceof EmptyStmt ? null : e;
	}

	public String toString() {
		return super.toString()+"if (" + cond.toString() + ")\n" + ifs.toString()
				+ (elses != null ? (super.toString()+"else\n"	+ elses.toString()) : "");
	}

	@Override
	public boolean empty() {
		return false;
	}

	@Override
	public void simplify() {
		ifs.simplify();
		if(elses!=null)
			elses.simplify();
	}

	@Override
	protected void setDepth(int d) {
		depth=d;
		ifs.setDepth(ifs instanceof BlockStmt?d:d+1);
		if(elses!=null)
			elses.setDepth(elses instanceof BlockStmt?d:d+1);;
	}
	
	@Override
	public boolean contains(String str) {
		if ((cond!=null) && cond.contains(str))
			return true;
		if ((ifs!=null) && ifs.contains(str))
			return true;
		if ((elses!=null) && elses.contains(str))
			return true;
		return false;
	}	
	
}
