package en.ast;

public class BoolLiteral extends Literal {
	boolean value;
	
	public BoolLiteral(boolean b) {
		value=b;
	}
	
	boolean value() {
		return value;
	}
	
	public String toString() {
		return value?"true":"false";
	}
	
	@Override
	public boolean contains(String str) {
		return false;
	}
}
