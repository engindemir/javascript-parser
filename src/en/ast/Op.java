package en.ast;

public class Op {

	private String opname;

	public Op(String name) {
		opname = name;
	}

	public String toString() {
		return opname;
	}

}
