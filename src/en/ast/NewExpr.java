package en.ast;

import java.util.*;

public class NewExpr extends Expr {
	private Expr base;
	private List<Expr> args = new ArrayList<Expr>();

	public NewExpr(Expr b, List<Expr> a) {
		args.addAll(a);
		base = b;
	}

	public String toString() {
		String temp = "new " + base.toString() + "(";
		for (int x = 0; x < args.size(); x++) {
			if (x == 0) {
				temp = temp + args.get(x).toString();
			} else {
				temp = temp + "," + args.get(x).toString();
			}
		}
		return temp + ")";
	}


	@Override
	protected void setDepth(int d) {
		depth=d;
		base.setDepth(d);
		for(Expr a:args)
			a.setDepth(d);
	}
	
	@Override
	public boolean contains(String str) {
		if ((base!=null)&&base.contains(str))
			return true;
		for(Expr a:args) {
			if (a.contains(str))
				return true;
		}
		return false;
	}		
}
