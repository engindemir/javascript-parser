package en.ast;

public class MemberExpr extends Expr implements LValue {
	private Expr b;
	private Expr p;

	public MemberExpr(Expr b1, Expr p1) {
		b = b1;
		p = p1;
	}

	public String toString() {
		return b.toString() + "." + (p instanceof StringLiteral?((StringLiteral) p).value():p.toString());
	}

	@Override
	protected void setDepth(int d) {
		depth=d;
		b.setDepth(d);
		p.setDepth(d);
	}
	
	@Override
	public boolean contains(String str) {
		if ((b!=null)&&b.contains(str))
			return true;
		if ((p!=null)&&p.contains(str))
			return true;
		return false;
	}		
}
