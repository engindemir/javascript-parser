package en.ast;

public class AssignExpr extends Expr {
	private LValue v;
	private Expr exp;
	private Op op;

	public AssignExpr(Op o, LValue l, Expr e) {
		v = l;
		exp = e;
		op = o;
	}

	@Override
	public String toString() {
		return v.toString() + op + exp.toString();
	}
	
	@Override
	protected void setDepth(int d) {
		depth=d;
		exp.setDepth(d);
	}
	
	@Override
	public boolean contains(String str) {
		if ((v!=null) && v.contains(str))
			return true;
		if ((exp!=null)&& exp.contains(str))
			return true;
		return false;
	}
	
}
