package en.ast;

public class ThisExpr extends Expr implements LValue {
	public ThisExpr() {
	}

	public String toString() {
		return "this";
	}

	@Override
	protected void setDepth(int d) {
		depth=d;
	}
	
	@Override
	public boolean contains(String str) {
		return false;
	}	

}
