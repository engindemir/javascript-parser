package en.ast;

public abstract class ASTNode {
	private String source;
	private int startLine;
	private int startColumn;
	private int endLine;
	private int endColumn;
	
	protected int depth;
	
	abstract protected void setDepth(int d);
	
	public abstract boolean contains(String str);
	
	//public abstract ASTNode hasSubASTNode(ASTNode ast);
	
	public void setLocation(String s, int sl, int sc, int el, int ec) {
		source=s;
		startLine=sl;
		startColumn=ec;
		endLine=el;
		endColumn=ec;
	}

	public String getSource() {
		return source;
	}
	
	public int getEndColumn() {
		return endColumn;
	}

	public int getEndLine() {
		return endLine;
	}

	public int getStartColumn() {
		return startColumn;
	}

	public int getStartLine() {
		return startLine;
	}
	
	
	public abstract void simplify();
	
}
