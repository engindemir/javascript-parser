package en.ast;

public class EmptyStmt extends Stmt {
	public String toString() {
		return super.toString()+";\n";
	}
	
	@Override
	public void simplify() {}

	@Override
	public boolean empty() {
		return true;
	}

	@Override
	protected void setDepth(int d) {
		depth=d;
	}
	
	@Override
	public boolean contains(String str) {
		return false;
	}	
}
