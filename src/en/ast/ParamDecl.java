package en.ast;

import java.util.*;

public class ParamDecl extends ASTNode {
	private List<String> params = new ArrayList<String>();

	public ParamDecl(List<String> l) {
		params.addAll(l);
	}

	public String toString() {
		String temp = "";
		for (int x = 0; x < params.size(); x++) {
			if (x == 0) {
				temp = temp + params.get(x).toString();
			} else {
				temp = temp + "," + params.get(x).toString();
			}
		}
		return temp;
	}

	@Override
	public void simplify() {
	
	}
	
	@Override
	protected void setDepth(int d) {
		depth=d;
	}
	
	@Override
	public boolean contains(String str) {
		if (params.contains(str))
			return true;
		return false;
	}		
}
