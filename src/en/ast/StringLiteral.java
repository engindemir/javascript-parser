package en.ast;

public class StringLiteral extends Literal {
	private String value;

	public StringLiteral(String v) {
		value = v;
	}

	public String value() {
		return value;
	}
	
	public String toString() {
		return "\""+value+"\"";
	}
	
	@Override
	public boolean contains(String str) {
		if (value.compareTo(str)==0)
			return true;
		return false;
	}	
}
