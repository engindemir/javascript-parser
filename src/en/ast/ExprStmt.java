package en.ast;

public class ExprStmt extends Stmt {
	private Expr exp;

	public ExprStmt(Expr e) {
		exp = e;
	}

	public String toString() {
		return super.toString()+exp.toString() + (exp instanceof FunctionDecl?"\n":";\n");
	}

	@Override
	public boolean empty() {
		return false;
	}

	@Override
	public void simplify() {
		exp.simplify();
	}

	@Override
	protected void setDepth(int d) {
		depth=d;
		exp.setDepth(d);
	}
	
	@Override
	public boolean contains(String str) {
		if ((exp!=null)&&exp.contains(str))
			return true;
		return false;
	}
	
	
}
