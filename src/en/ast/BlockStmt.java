package en.ast;

import java.util.*;

public class BlockStmt extends Stmt {
	private List<Stmt> members = new ArrayList<Stmt>();

	public BlockStmt(List<Stmt> m) {
		members.addAll(m);
	}

	public String toString() {
		String temp = "{\n";
		for (int x = 0; x < members.size(); x++) {
			temp = temp + (members.get(x).toString());
		}
		return super.toString()+temp + super.toString()+"}\n";
	}

	@Override
	public void simplify() {
		for(int i=0; i<members.size(); ++i) {
			members.get(i).simplify();
			if(members.get(i).empty()) {
				members.remove(i);
				i--;
			}
		}
	}
	
	public boolean empty()
	{
		return members.isEmpty();
	}
	
	@Override
	protected void setDepth(int d) {
		depth=d;
		for(Stmt s:members)
			s.setDepth(d+1);
	}
	
	@Override
	public boolean contains(String str) {
		for(Stmt s:members){
			if (s.contains(str))
				return true;
		}
		return false;
	}	
	
}
