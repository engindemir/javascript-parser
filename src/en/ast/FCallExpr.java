package en.ast;

import java.util.*;

public class FCallExpr extends Expr implements LValue {
	private Expr b;
	private ArrayList<Expr> args = new ArrayList<Expr>();

	public FCallExpr(Expr fun, List<Expr> l) {
		args.addAll(l);
		b = fun;
	}

	public String toString() {
		String temp = "";
		for (int x = 0; x < args.size(); x++) {
			if ((x == 0)) {
				temp = temp + args.get(x).toString();
			} else {
				temp = temp + "," + args.get(x).toString();
			}
		}
		return b.toString() + "(" + temp + ")";
	}

	@Override
	protected void setDepth(int d) {
		depth=d;
		b.setDepth(d);
		for(Expr e:args)
			e.setDepth(d);
	}
	
	@Override
	public boolean contains(String str) {
		if ((b!=null)&& b.contains(str))
			return true;
		for(Expr e:args){
			if (e.contains(str))
				return true;
		}
		return false;
	}	
	
}
