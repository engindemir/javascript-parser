package en.ast;

public class UnaryExpr extends Expr {

	private Op theop;
	private Expr ex;

	public UnaryExpr(Op o, Expr e) {
		theop = o;
		ex = e;
	}

	public String toString() {
		return theop.toString() + (ex instanceof Literal?ex:"(" + ex + ")");
	}

	@Override
	protected void setDepth(int d) {
		depth=d;
		ex.setDepth(d);
	}
	
	@Override
	public boolean contains(String str) {
		if ((ex!=null)&&ex.contains(str))
			return true;
		return false;
	}	
}
