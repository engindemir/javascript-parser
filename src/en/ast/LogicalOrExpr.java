package en.ast;

public class LogicalOrExpr extends Expr {
	private Expr lexpr;
	private Expr rexpr;

	public LogicalOrExpr(Expr l, Expr r) {
		lexpr = l;
		rexpr = r;
	}

	public String toString() {
		return lexpr.toString() + " || " + rexpr.toString();
	}
	
	@Override
	protected void setDepth(int d) {
		depth=d;
		lexpr.setDepth(d);
		rexpr.setDepth(d);
	}

	@Override
	public boolean contains(String str) {
		if ((lexpr!=null)&&lexpr.contains(str))
			return true;
		if ((rexpr!=null)&&rexpr.contains(str))
			return true;
		return false;
	}		
	
}
