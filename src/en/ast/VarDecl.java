package en.ast;

import java.util.ArrayList;
import java.util.List;

public class VarDecl extends Expr {
	List<AssignExpr> ae = new ArrayList<AssignExpr>();
	
	public VarDecl(List<AssignExpr> l) {
		ae.addAll(l);
	}
	
	public String toString()
	{
		String temp = "var ";
		boolean first=true;
		for(AssignExpr a:ae) {
			if(!first)
				temp+=",";
			temp=temp + a;
			first=false;
		}
		return temp;
	}
	
	@Override
	protected void setDepth(int d) {
		depth=d;
		for(AssignExpr e:ae)
			e.setDepth(d);
	}
	
	@Override
	public boolean contains(String str) {
		for(AssignExpr e:ae) {
			if (e.contains(str))
				return true;
		}
		return false;
	}	
}
