package en.ast;

import java.util.*;

public class Program extends ASTNode {
	private List<Stmt> members=new ArrayList<Stmt>();

	private HashMap<String, ArrayList<ASTNode>> ASTObjects; 
	
	public Program(List<Stmt> m) {
		members.addAll(m);
		setDepth(0);
		
		ASTObjects = new HashMap<String, ArrayList<ASTNode>>();
	}
	
	public String toString() {
		String temp = "";
		for (int x = 0; x < members.size(); x++) {
			temp = temp + (members.get(x).toString());
		}
		return temp;
	}

	@Override
	public void simplify() {
		for (int i = 0; i < members.size(); ++i) {
			members.get(i).simplify();
			if (members.get(i).empty()) {
				members.remove(i);
				i--;
			}
		}
	}

	@Override
	protected void setDepth(int d) {
		depth=d;
		for(Stmt s: members)
			s.setDepth(d);
	}
	
	@Override
	public boolean contains(String str) {
		for(Stmt s: members) {
			if (s.contains(str))
				return true;
		}
		return false;
	}
	
	public HashMap<String, ArrayList<ASTNode>> getASTObjects(){
		return ASTObjects;
	}
	
	
	public void setASTObjects(ASTNode ast){
		
		// Get the unqualified name of a class
		String name = ast.getClass().getName();
		if (name.lastIndexOf('.') > 0) {
		    name = name.substring(name.lastIndexOf('.')+1);
		}
		
		ArrayList<ASTNode> nl = ASTObjects.get(name);
		if (nl != null){
			nl.add(ast);
		}
	}
	
	public void setASTObjects(HashMap<String, ArrayList<ASTNode>> idxAST){
		ASTObjects.putAll(idxAST);
	}	
	
	
}
