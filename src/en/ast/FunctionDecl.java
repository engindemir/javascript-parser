package en.ast;

public class FunctionDecl extends Expr {
	private IdExpr ide;
	private ParamDecl pd;
	private Stmt bodystmt;

	public FunctionDecl(IdExpr i, ParamDecl p, Stmt s) {
		ide = i;
		pd = p;
		bodystmt = s;
	}

	public String toString() {
		return "function " + (ide!=null?ide.toString():"") + "(" + pd.toString() + ")\n"
				+ bodystmt.toString();
	}
	
	@Override
	public void simplify() {
		bodystmt.simplify();
	}
	
	@Override
	protected void setDepth(int d) {
		depth=d;
		bodystmt.setDepth(bodystmt instanceof BlockStmt?d:d+1);
	}
	
	@Override
	public boolean contains(String str) {
		if ((ide != null) && ide.contains(str))
			return true;
		if ((pd != null) && pd.contains(str))
				return true;
		if ((pd != null) && bodystmt.contains(str))
			return true;
		return false;
	}	
}
