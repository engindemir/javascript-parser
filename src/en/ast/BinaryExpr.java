package en.ast;

public class BinaryExpr extends Expr {
	private Op o;
	private Expr lexpr;
	private Expr rexpr;

	public BinaryExpr(Op o2, Expr l, Expr r) {
		lexpr = l;
		o = o2;
		rexpr = r;
	}

	public String toString() {
		return lexpr.toString() + o.toString() + rexpr.toString();
	}
	
	@Override
	protected void setDepth(int d) {
		depth=d;
		lexpr.setDepth(d);
		rexpr.setDepth(d);
	}
	
	@Override
	public boolean contains(String str) {
		if ((lexpr!=null)&&lexpr.contains(str))
			return true;
		if ((rexpr!=null)&&rexpr.contains(str))
			return true;
		return false;
	}	
}
