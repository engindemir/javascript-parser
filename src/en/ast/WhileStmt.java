package en.ast;

public class WhileStmt extends Stmt {

	private Expr cond;
	private Stmt body;

	public WhileStmt(Expr c, Stmt s) {
		cond = c;
		body = s;
	}

	public String toString() {
		return super.toString()+"while(" + cond.toString() + ") {" + body.toString() + "}";
	}

	@Override
	public boolean empty() {
		return false;
	}

	@Override
	public void simplify() {
		body.simplify();
	}

	@Override
	protected void setDepth(int d) {
		depth=d;
		body.setDepth(body instanceof BlockStmt?d:d+1);
	}
	
	@Override
	public boolean contains(String str) {
		if ((cond!=null)&&cond.contains(str))
			return true;
		if ((body!=null)&&body.contains(str))
				return true;
		
		return false;
	}	
}
