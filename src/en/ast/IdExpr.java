package en.ast;

public class IdExpr extends Expr implements LValue {
	private String name;

	public IdExpr(String n) {
		name = n;
	}

	public String toString() {
		return name;
	}

	@Override
	protected void setDepth(int d) {
		depth=d;
	}
	
	@Override
	public boolean contains(String str) {
		if (name.compareTo(str)==0)
			return true;
		return false;
	}	
	
}

