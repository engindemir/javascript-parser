package en.ast;

public abstract class Stmt extends ASTNode {
	abstract public boolean empty();
	
	public String toString() {
		String temp="";
		for(int i=0;i<depth;++i)
			temp+="  ";
		return temp;
	}
}
