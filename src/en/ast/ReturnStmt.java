package en.ast;

public class ReturnStmt extends Stmt {

	private Expr ex;

	public ReturnStmt(Expr e) {
		ex = e;
	}

	public String toString() {
		return super.toString()+"return " + ex.toString() +";\n";
	}

	@Override
	public boolean empty() {
		return false;
	}

	@Override
	public void simplify() {
	}

	@Override
	protected void setDepth(int d) {
		depth=d;
	}
	
	@Override
	public boolean contains(String str) {
		if ((ex!=null)&&ex.contains(str))
			return true;
		return false;
	}	
}
