package en.ast;

public class NumericLiteral extends Literal {
	int value;
	
	public NumericLiteral(int v) {
		value=v;
	}
	
	int value() {
		return value;
	}
	
	public String toString() {
		return new Integer(value).toString();
	}
	
	@Override
	public boolean contains(String str) {
		return false;
	}
}
