package en.ast;

public class ForStmt extends Stmt {
	private Expr initialexpr;
	private Expr terminalexpr;
	private Expr increment;
	private Stmt body;

	public ForStmt(Expr i, Expr t, Expr inc, Stmt b) {
		initialexpr = i;
		terminalexpr = t;
		increment = inc;
		body = b;
	}

	public String toString() {
		return super.toString()+"for (" + initialexpr.toString() + ";" + terminalexpr.toString()
				+ ";" + increment.toString() + ")\n" + body.toString() + "";
	}

	@Override
	public boolean empty() {
		return false;
	}

	@Override
	public void simplify() {
		body.simplify();
	}

	@Override
	protected void setDepth(int d) {
		depth=d;
		body.setDepth(body instanceof BlockStmt?d:d+1);
	}
	
	@Override
	public boolean contains(String str) {
		if ((initialexpr != null) && initialexpr.contains(str))
			return true;
		if ((terminalexpr != null) && terminalexpr.contains(str))
				return true;
		if ((increment != null) && increment.contains(str))
			return true;
		if ((body != null) && body.contains(str))
			return true;
		return false;	

	}	
	
}
