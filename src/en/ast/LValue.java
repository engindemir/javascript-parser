package en.ast;

public interface LValue {
	
	public boolean contains(String name);
	
}
