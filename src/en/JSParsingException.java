package en;

/**
 * Is thrown when a parsing error occurred during parsing
 * 
 * @see JSParser
 * 
 */
public class JSParsingException extends Exception {
	private static final long serialVersionUID = -7979819409465296525L;

	/**
	 * Constructor with message
	 * 
	 * @param message
	 *            description of the parsing error
	 */
	public JSParsingException(String message) {
		super(message);
	}

	/**
	 * Constructor with message and chained exception
	 * 
	 * @param message
	 *            description of the parsing error
	 * @param cause
	 *            originating exception
	 */
	public JSParsingException(String message, Throwable cause) {
		super(message, cause);
	}
}