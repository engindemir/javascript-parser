package en.test;
import java.io.BufferedReader;
import java.io.FileReader;
import java.net.URL;

import org.w3c.dom.Document;

import en.ASTBuilder;
import en.JS2AST;
import en.Utils;
import en.ast.Program;


public class Test {

	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub

		//final URL codeURL = new URL("http://www.savills.co.uk/_js/common.js");
		
		BufferedReader in = new BufferedReader(new FileReader("jslib/parse2AST.js"));
		
		JS2AST jp = new JS2AST();
		//System.out.print(p.createAST(codeURL));
		//Document d = jp.createAST(in,"test/toto.js");
		
		Document d = jp.createAST(in,new URL("http://www.savills.co.uk/_js/common.js"));
		
		//Utils.printXML(d);		
		Program p = ASTBuilder.buildFromXML(d);	
		System.out.print(p);
		
	}

}
