package en;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.net.URL;
import java.net.URLConnection;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;

public class Utils {

	/**
	 *  Returns the data from URL as String
	 *  
	 * @param URL	URL address of the source	
	 * @throws IOException
	 */	
	public static String getURLContent(URL u) throws IOException
	{
		URLConnection uc=u.openConnection();
		StringBuffer buffer = new StringBuffer();
		
		BufferedReader reader = new BufferedReader(new InputStreamReader(uc.getInputStream()));
		String line;
		while((line=reader.readLine())!=null) {
			buffer.append(line+"\n");
		}
		return buffer.toString();
	}	

	/**
	 *  Returns the data from file as String
	 *  
	 * @param filename		Full path of the file
	 */
	public static String readFile2String(String filename){
		StringBuffer buffer = new StringBuffer();
		
		BufferedReader reader;
		try {
			reader = new BufferedReader(new FileReader(filename));
			String line;
			while((line=reader.readLine())!=null) {
				buffer.append(line+"\n");
			}			
		} catch (IOException e) {
			throw new RuntimeException("Error loading file: " + filename, e);
		}

		return buffer.toString();
	}
	
	public static void printXML(Document doc) throws Exception{
		Transformer transformer = TransformerFactory.newInstance().newTransformer();
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");

		//initialize StreamResult with File object to save to file
		StreamResult result = new StreamResult(new StringWriter());
		DOMSource source = new DOMSource(doc);
		transformer.transform(source, result);

		String xmlString = result.getWriter().toString();
		System.out.println(xmlString);		
		
	}
}
